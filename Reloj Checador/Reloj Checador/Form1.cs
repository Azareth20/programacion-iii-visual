﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reloj_Checador
{
    public partial class Form1 : Form
    {
       
        DateTime datoEntrada = new DateTime();
        DateTime d = new DateTime();
        DateTime datoSalida = new DateTime();
        string e;

        //banderas de prueba
        bool r1 = false;
        bool r2 = false;
        int flag = 0;

        public string Path = @"C:\Users\nazareno\Desktop\UNEDL 2020\Programacion VIsual III\RegistrodeEmpleados.txt";



        public Form1()
        {
            InitializeComponent();
        }


        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            string text = comboBox1.Text;
            

            if (!File.Exists(Path))
            {
                using (StreamWriter sw = File.CreateText(Path))
                {
                    sw.WriteLine(e);
                    sw.WriteLine(datoEntrada);

                }
            }
            else
            {
                string append = "";
                if (flag == 1)
                {
                    append = "Entrada\n" + e + "\n" + datoEntrada + Environment.NewLine;
                }
                else if (flag == 2)
                {
                    append = "Salida\n" + e + "\n" + datoSalida + Environment.NewLine;
                }

                File.AppendAllText(Path, append);
                datoEntrada = d;

            }



        }

        private void btnRevisar_Click(object sender, EventArgs e)
        {

            string readText = File.ReadAllText(Path);
            MessageBox.Show(readText);
        }

        private void rbEntrada_CheckedChanged(object sender, EventArgs e)
        {
            btnRegistrar.Enabled = true;
            btnRevisar.Enabled = true;
            r1 = true;
            flag = 1;
        }

        private void rbSalida_CheckedChanged(object sender, EventArgs e)
        {
            datoSalida = dateTimePicker1.Value;
            btnRegistrar.Enabled = true;
            btnRevisar.Enabled = true;
            r1 = true;
            flag = 2;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            datoEntrada = dateTimePicker1.Value;
        }
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
