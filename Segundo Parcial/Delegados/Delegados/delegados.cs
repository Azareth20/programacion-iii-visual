﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Azareth Mtz

namespace Delegados
{
    delegate int Resultado(int x1, int x2);

    class MiDelegado
    {
        public int Suma(int a, int b)
        {
            return a + b;
        }

        public int Resta(int a, int b)
        {
            return a - b;
        }

        static void Main(string[] args)
        {
            MiDelegado p = new MiDelegado();
            Console.WriteLine("Suma y resta de dos valores.");
            Console.WriteLine(p.Suma(10, 5));
            Console.WriteLine(p.Resta(10, 5));
            Resultado delegado = p.Suma;

            Console.WriteLine("Suma y resta de dos valores llamando a los métodos a través de Delegados");
            Console.WriteLine(delegado(10, 5));
            delegado = p.Resta;
            Console.WriteLine(delegado(10, 5));
            Console.ReadKey();
        }
    }
}
