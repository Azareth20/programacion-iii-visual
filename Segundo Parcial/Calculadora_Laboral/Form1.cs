﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora_Laboral
{
    public partial class Form1 : Form
    {
        float sueldoM, prestaciones, segurovida, sueldoneto;

        public Form1()
        {
            
            InitializeComponent();
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            sueldoM = float.Parse(txtSM.Text);
            prestaciones = float.Parse(txtP.Text);
            segurovida = float.Parse(txtSV.Text);
            sueldoneto = sueldoM + prestaciones + segurovida;
            txtVista.Text = sueldoneto.ToString();
       
        }
        private void btnL_Click(object sender, EventArgs e)
        {
            txtSM.Clear();
            txtP.Clear();
            txtSV.Clear();
            txtVista.Text = "";
        }
        private void btnSa_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
