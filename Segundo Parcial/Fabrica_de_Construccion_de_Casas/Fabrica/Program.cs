﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Construccion
{
    class Program
    {
        private static Semaphore pooldechalanes;
        private static int _padding;
        private static int n = 0;
        private static int m = 0;
        private static int o = 0;
        private static int c = 0;
        private static int b = 0;
        private static int a = 0;
        private static void ChalanAlbanil(object Comp)
        {
            Console.WriteLine("Compañero del Albañil");
            Console.WriteLine("Thread {0}: {1}, Priority {2}",
                        Thread.CurrentThread.ManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            Console.WriteLine("El Chalan esta {0} esperando su turno...", Comp);
            pooldechalanes.WaitOne();
            // A padding interval to make the output more orderly.
            int padding = Interlocked.Add(ref _padding, 100);
            Console.WriteLine("Ayudante {0} obtiene su turno...", Comp);
            Thread.Sleep(1000 + padding);
            Console.WriteLine("Ayudante {0} termina su turno...", Comp);
            Console.WriteLine("Ayudante {0} libera su lugar {1}",
                Comp, pooldechalanes.Release());
        }

        public static async void ConstruirCasas(int Cantd, int Habitaciones, int Baños)
        {
            Random rand = new Random();
            Task[] Casa = new Task[Cantd];
            int cont = 0;
            Action<object> action = (object obj) =>
            {
                Console.WriteLine(" La casa {0} esta lista para construir ", Task.CurrentId);
            };

            for (int i = 0; i < Casa.Length; i++)
            {

                Casa[i] = new Task(action, new string('1', i + 1));
                Console.WriteLine("Empezando la casa {0} ", Task.CurrentId);
                if (cont != 0)
                {
                    Habitaciones = rand.Next(1, 10);
                    Baños = rand.Next(1, 5);

                }
                for (int j = 0; j < Habitaciones; j++)
                {
                    Console.WriteLine(" creando habitaciones ");
                }
                for (int b = 0; b < Baños; b++)
                {
                    Console.WriteLine(" creando baños");
                }
                Casa[i].Start();
                Thread.Sleep(500);
                cont++;


            }

            Console.WriteLine("Casas finalizadas");

            await Task.WhenAll(Casa);

        }


        public static void Main(string[] args)
        {
            Console.WriteLine("¿Cuántos personas llegaron a trabajar?");
            n = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántas personas pueden trabajar?");
            m = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos segundos hay para contruir?");
            o = Convert.ToInt32(Console.ReadLine()) * 1000;

            Console.WriteLine("¿Cuantas casas se construiran?");
            c = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuantas Habitaciones se van a construir?");
            a = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuantas baños se van a construir?");
            b = Convert.ToInt32(Console.ReadLine());

            pooldechalanes = new Semaphore(0, m);


            Console.WriteLine("Iniciando con la construcción");

            for (int i = 1; i <= m; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(ChalanAlbanil));
                if (i % 2 == 0)
                {
                    t.Priority = ThreadPriority.AboveNormal;
                }
                else
                {
                    t.Priority = ThreadPriority.BelowNormal;
                }
                t.Start(i);
            }
            ConstruirCasas(c, a, b);

            Thread.Sleep(500);
            Console.WriteLine("Se libero un turno");
            pooldechalanes.Release(m);


            Thread.Sleep(o);
            Console.WriteLine("Se ha finalizado la construcción");
        }
    }
}