﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Carreras
{
    class CTaskDos
    {
        List<Task> tareas;
        int longitud;
        Random c;
        List<Task> resultado;
        public CTaskDos(int pLongitud)
        {
            tareas = new List<Task>();
            longitud = pLongitud;
            c = new Random();
        }



        public void proceso()
        {
            for (int i = 0; i < longitud; i++)
            {
                tareas.Add(Task.Run(() => Thread.Sleep(c.Next(10, 100))));
            }

            if (Task.WhenAny(tareas) == Task.CompletedTask)
            {
                Console.WriteLine("El carro ganador fue el {0}", Task.CurrentId);
            }


            if (Task.WhenAll(tareas).IsCompleted == true)
            {
                Task.WhenAll(tareas.ToArray());

                foreach (Task t in tareas)
                {
                    Console.WriteLine("coches : {0}", t.Id);
                }
            }

            //Task.WhenAll(tareas.ToArray());

            //foreach (Task t in tareas)
            //{
            //    Console.WriteLine("coches : {0}", t.Id);
            //}
        }


    }
}

