﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        double primero = 0;
        double segundo = 0;
        string operacion;
        bool flag = false; 
        double resultado;

        public Form1()
        {
            InitializeComponent();
        }
       

        private void btn1_Click(object sender, EventArgs e)
        {

            txtResultado.Text = txtResultado.Text + "1";

        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
           txtResultado.Text = txtResultado.Text + "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "9";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
             txtResultado.Text += "0";
        }

        private void btnPunto_Click(object sender, EventArgs e)
        {
            txtResultado.Text = ".";
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        private void btnSuma_Click(object sender, EventArgs e)
        {
            operacion = "+";
            primero = primero + double.Parse(txtResultado.Text);
            txtResultado.Clear();
        }

        private void btnResta_Click(object sender, EventArgs e)
        {
            operacion = "-";
            primero = primero + double.Parse(txtResultado.Text);
            txtResultado.Clear();
        }

        private void lblM_Click(object sender, EventArgs e)
        {
            operacion = "*";
            primero = primero + double.Parse(txtResultado.Text);
            txtResultado.Clear();
        }

        private void btnEntre_Click(object sender, EventArgs e)
        {
            operacion = "/";
            primero = primero + double.Parse(txtResultado.Text);
            txtResultado.Clear();
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            segundo = double.Parse(txtResultado.Text);
            switch (operacion){
                case "+":
                    resultado = primero + segundo;
                    txtResultado.Text = resultado.ToString();
                    primero = 0;
                    segundo = 0;
                    break;

                case "-":
                    resultado = primero - segundo;
                    txtResultado.Text = resultado.ToString();
                    primero = 0;
                    segundo = 0;
                    break;

                case "*":
                    resultado = primero * segundo;
                    txtResultado.Text = resultado.ToString();
                    primero = 0;
                    segundo = 0;
                    break;

                case "/":
                    if (primero == 0||segundo == 0||primero == 0 && segundo == 0 )
                    {
                        MessageBox.Show("No se puede");

                    }
                    else
                    {


                        resultado = primero / segundo;
                        txtResultado.Text = resultado.ToString();
                        primero = 0;
                        segundo = 0;
                    }
                    break;
            }
                  
            }

        private void btnC_Click(object sender, EventArgs e)
        {
            txtResultado.Clear();
        }

        private void btnBorrar1_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text == String.Empty)
            {
                MessageBox.Show("No puede borrar si no hay nada");
                flag = false;
            }
            else
            {
                if (txtResultado.Text.Contains("0") || txtResultado.Text.Contains("1") || txtResultado.Text.Contains("2") || txtResultado.Text.Contains("3") ||
                    txtResultado.Text.Contains("4") || txtResultado.Text.Contains("5") || txtResultado.Text.Contains("6") || txtResultado.Text.Contains("7") ||
                    txtResultado.Text.Contains("8") || txtResultado.Text.Contains("9") || txtResultado.Text.Contains(".") || txtResultado.Text.Contains("+") ||
                    txtResultado.Text.Contains("-") || txtResultado.Text.Contains("*") || txtResultado.Text.Contains("/"))
                {
                    txtResultado.Text = txtResultado.Text.Remove(txtResultado.Text.Length - 1);

                    if(txtResultado.Text == String.Empty)
                    {
                        flag = false;
                    }


                }
            }
        }
        private void btnmasmenos_Click(object sender, EventArgs e)
        {

        }
    }
    }

